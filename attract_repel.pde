import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;
import TUIO.*;



Music musicFile;
String backgroundImageFile = "/./images/BG_Small_01.svg";
String boidShapeFile = "/images/boid.svg";

//TODO add controlP5 for configuration of values

//add global variables here
//-----------deep space------------
int scaleFactor = 1; //ACHTUNG nur skalierungsfaktor ändern!!!

//##Variablen sind tabu##
int windowWidth = 3030/scaleFactor; // for real Deep Space this should be 3030
int windowHeight = 3712/scaleFactor; // for real Deep Space this should be 3712
int wallHeight = 1914/scaleFactor; // for real Deep Space this should be 1914 (Floor is 1798)


PharusClient pc;
PFont font;
//-----------deep space------------

//-----------graphics --------
PGraphics pg;
PGraphics flockPg;
//-----------graphics --------

//------ our defined globals ------
PShape boidShape;
Flock flock; // manages swarm
IslandHandler islandHandler; //manages islands
Visualizer vis; // handles visualisation
IslandCreator islandCreator; // handles creation of islands
EffectHandler effectHandler;

Minim minim; //music recognition
AudioPlayer song;
AudioAnalyzer audioAnalyzer;
Utils util;

boolean shouldAttract = true;
//------ our defined globals ------

void settings()
{
 //size(windowWidth, windowHeight, P3D);
 fullScreen(P3D, SPAN);  
}

void setup() {
  //frameRate(30);
  musicFile = you;
  pg = createGraphics(windowWidth, wallHeight);
  flockPg = createGraphics(windowWidth, wallHeight, P3D);
  pg.smooth(1); //TODO wenn performance einbricht das hier wegnehmens
  //pg.hint(DISABLE_DEPTH_TEST);
  //pg.hint(DISABLE_OPENGL_ERRORS);
  //pg.hint(DISABLE_DEPTH_MASK);
  //pg.hint(DISABLE_DEPTH_TEST);
  
  flockPg.smooth(1); 
  //flockPg.hint(DISABLE_DEPTH_TEST);
  //flockPg.hint(DISABLE_OPENGL_ERRORS);
  //flockPg.hint(DISABLE_DEPTH_MASK);
  //flockPg.hint(DISABLE_DEPTH_TEST);
 
  
  util = new Utils();

  initFont();
  initAudioAnalyzer();
  initPlayerTracking();
  initFlock();
  initVisualizer();
  initIslandHandler();
  initIslandCreator();
  initEffectHandler();
  background(0, 0, 0); // Change color of background -  TODO ramp in bg wich reacts to music
}

void draw() {
  thread("analyzeMixInThread");
  thread("calculatePositionsInThread");
  background(0, 0, 0); // Change color of background -  TODO ramp in bg wich reacts to music
  effectHandler.run();
  
  
  pg.beginDraw();
  pg.colorMode(HSB); //uncomment to use hsb format in visualizer
  pg.background(0,0);
  vis.visualize();
  pg.colorMode(RGB);

  // drawPlayerTracking(); //TODO ausstellen wenn in visualizer player gezeichnet werden

  pg.endDraw();

  //actual drawing of the windows (duplicated)
  pushMatrix();
    translate(windowWidth / 2, wallHeight / 2, 0);
    scale(1, -1, 1);
    translate(-windowWidth / 2, -wallHeight / 2, 0);
    image(flockPg, 0,0);
    image(pg, 0, 0); //draws upper half
  popMatrix();
  //draw3DRoom();
  pushMatrix();
    translate(0, wallHeight);
    image(flockPg, 0,0);
    image(pg, 0, 0); //draws lower half
  popMatrix();
  //audioAnalyzer.draw(); //debugging audio scores //TODO remove audio analyzer
  //drawFPS(); //debugging frame count

}

//keyboard inputs
void keyPressed() {
  if (keyCode == 39) {
    fastForward();
  } else if (keyCode == 37) {
    fastBackward();
  }
}

private void fastForward() {
  song.skip(5000);
}

private void fastBackward() {
  song.skip(-5000);
}

private void initFont() {
  font = createFont("Arial", 12);
  textFont(font, 12);
  textAlign(CENTER, CENTER);
}

private void initFlock() {
  flock = new Flock();
  for (int i = 0; i < 1050; i++) { //1000
    Boid b = new Boid(windowWidth/2 + (int)random(5) - 2, wallHeight/2 + (int)random(5) - 2);
    flock.addBoid(b);
  }
}

private void initAudioAnalyzer() {
  minim = new Minim(this);
  song = minim.loadFile(musicFile.musicFile);
  song.play(0);
  audioAnalyzer = new AudioAnalyzer(song);
}

private void initVisualizer() {
  vis = new Visualizer(audioAnalyzer, flock);
}

private void initIslandHandler() {
  islandHandler = new IslandHandler();
  //islandHandler.addIsland(0 ,50,160,60,30,0.5);
  //  islandHandler.addIsland(1 ,200,50,100,30,0.5);
  //    islandHandler.addIsland(0 ,50,300,60,30,0.5);

}

private void initEffectHandler() {
  effectHandler = new EffectHandler();
}

private void initIslandCreator() {
  islandCreator = new IslandCreator();
}

private void draw3DRoom() {
  pushMatrix();
  rotateX(-HALF_PI);
  image(pg, 0, 0);
  popMatrix();
  pushMatrix();
  rotateY(HALF_PI);
  image(pg, 0, 0);
  popMatrix();
  pushMatrix();
  translate(0, 0, -windowWidth);
  image(pg, 0, 0);
  pushMatrix();
  translate(windowWidth, 0, 0);
  rotateY(-HALF_PI);
  image(pg, 0, 0);
  popMatrix();
  popMatrix();
}

void calculatePositionsInThread() {
  try {
    flock.run();
    islandHandler.run();
    islandCreator.run();
  } 
  catch (Throwable throwable) {
    println(throwable);
  }
}

void analyzeMixInThread() {
  try {
    audioAnalyzer.update(song.mix);
  } 
  catch (Throwable throwable) {
    println(throwable);
  }
}
