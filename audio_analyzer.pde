class AudioAnalyzer {

  FFT fft;
  float spectrum[];
  int fftSize;

  MusicScore score;
  MusicScore oldScore;

  Envelope env;
  final float amp = 4f;
  
  float scoreDecreaseRate = 1;


  AudioAnalyzer(AudioPlayer song) {
    init(song);
  }
  
   AudioAnalyzer(AudioPlayer song, float scoreDecreaseRate) {
    init(song);
    this.scoreDecreaseRate = scoreDecreaseRate;
  }


  void update(AudioBuffer mix) {
    oldScore = score;

    fft.forward(mix);

    resetScore();
    calculateAudioScores();
    dampenAudioScores();
  }

  void draw() {
    fill(255);
    for (int i  = 0; i < fftSize; i++) {
      if(i < fftSize / 3) {
        fill(255,200,200);
      } else if (i >= fftSize * 1/3 && i < fftSize * 2/3) {
        fill(200,255,200);
      } else {
        fill(200,200,255);
      }
      float x = map (i, 0, fftSize, 200, 600);
      rect(x, 50, 400 / fftSize, -spectrum[i] * amp);
    }
  }

  private void init(AudioPlayer song) {
    fft = new FFT (song.bufferSize(), song.sampleRate());
    fft.linAverages(20);                             //use averages ( in 30 values separated)
    fft.logAverages(22, 3);                           
    fftSize = fft.avgSize();
    spectrum = new float[fftSize];
    score = new MusicScore();
    oldScore = new MusicScore();
    env = new Envelope(song.bufferSize());
  }

  private void resetScore() {
    score.low = 0;
    score.mid = 0;
    score.high = 0;
  }

  private void calculateAudioScores() {
    for (int i = 0; i < fftSize; i++) {
      spectrum[i] += (fft.getAvg(i) * env.val[i] - spectrum[i]/4f);    //ease out
    }

    int border = fftSize / 3;

    for (int i = 0; i < border; i++) {
      score.low += spectrum[i];
    }

    int borderOld = border;
    border += border;

    for (int i = borderOld; i < border; i++) {
      score.mid += spectrum[i];
    }

    borderOld = border;
    border += border;

    for (int i = borderOld; i < fftSize; i++) {
      score.high += spectrum[i];
    }
  }

  private void dampenAudioScores() {
    if (oldScore.low > score.low) {
      score.low = oldScore.low - scoreDecreaseRate * 10;
    }

    if (oldScore.mid > score.mid) {
      score.mid = oldScore.mid - scoreDecreaseRate * 5;
    }

    if (oldScore.high > score.high) {
      score.high = oldScore.high - scoreDecreaseRate;
    }
  }
}

class Envelope {
  public float val[];
  float amp= 10f;


  Envelope(int fftSize) {
    val = new float[fftSize];
    for (int i = 0; i < val.length; i++) {
      val[i] = bezierPoint(
        0.02,
        0.5, 
        200, 
        400, 
        map(i, 0, val.length - 1, 0, 1));
    }
  }
}
