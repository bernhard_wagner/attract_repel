class Boid extends PositionObject {
  PVector velocity;
  PVector acceleration;
  float r;
  float maxforce;    // Maximum steering force
  float maxspeed;    // Maximum speed
  float attractorDistanceStrength = 0;
  float distanceThreshhold = 450 / scaleFactor;
  float desiredseparation = 125.0f / scaleFactor;
  float neighbordist = 150 / scaleFactor; //alignement and cohesion
  int distanceOffset = 200 / scaleFactor; //island separation
  PVector lastPosition;;

  Boid(int x, int y) {
    super(x,y);
    acceleration = new PVector(0,0);
    velocity = new PVector(random(-1,1),random(-1,1));
    r = 16 / scaleFactor;
    maxspeed = 5.6;
    maxforce = 0.045;
  }

  void run(Flock flock, List<Island> islands, HashMap<Long, Person> persons) {
    List<Boid> boids = flock.boids;
    flock(boids, islands, persons);
    update();
    borders();
  }

  void applyForce(PVector force) {
    // We could add mass here if we want A = F / M
    acceleration.add(force);
  }

  // We accumulate a new acceleration each time based on three rules
  void flock(List<Boid> boids, List<Island> islands, HashMap<Long, Person> persons) {
    PVector sep = separate(boids);   // Separation
    PVector ali = align(boids);      // Alignment
    PVector coh = cohesion(boids);   // Cohesion
    PVector attRep = attractRepell(persons);
    PVector islandSep = islandSeparation(islands);
    //TODO forces for islands and persons
    
    // Arbitrarily weight these forces
    sep.mult(1 + audioAnalyzer.score.high * 0.9); //TODO weight forces based on music
    ali.mult(1.25 + audioAnalyzer.score.high / 10);
    coh.mult(0.05 + audioAnalyzer.score.low / 5.1); //min(0.1 + audioAnalyzer.score.low / 10, 3)
    attRep.mult(0.6 + audioAnalyzer.score.mid / 40 + audioAnalyzer.score.high / 10);
    islandSep.mult(1.35 + audioAnalyzer.score.low / 5);
    // Add the force vectors to acceleration
    applyForce(sep);
    applyForce(ali);
    applyForce(coh);
    if(shouldAttract) {
       applyForce(attRep);
    }
    applyForce(islandSep);
  }

  // Method to update position
  void update() {
    maxspeed = min(max(0.5, audioAnalyzer.score.mid / 10), 11);
    // Update velocity
    acceleration.add(new PVector(0.1, 0.1));
    velocity.add(acceleration);
    // Limit speed
    velocity.limit(maxspeed);
    
    //update position
    position.add(velocity);
    // Reset accelertion to 0 each cycle
    acceleration.mult(0);
    
    //set old positions
    lastPosition = new PVector(position.x, position.y);
  }

  // A method that calculates and applies a steering force towards a target
  // STEER = DESIRED MINUS VELOCITY
  PVector seek(PVector target) {
    PVector desired = PVector.sub(target,position);  // A vector pointing from the position to the target
    // Normalize desired and scale to maximum speed
    desired.normalize();
    desired.mult(maxspeed);
    // Steering = Desired minus Velocity
    PVector steer = PVector.sub(desired,velocity);
    steer.limit(maxforce);  // Limit to maximum steering force
    return steer;
  }

  // Wraparound
  void borders() {
    if (position.x < -r) position.x = windowWidth+r;
    if (position.y < -r) position.y = wallHeight+r;
    if (position.x > windowWidth+r) position.x = -r;
    if (position.y > wallHeight+r) position.y = -r;
  }

  // Separation
  // Method checks for nearby boids and steers away
  PVector separate (List<Boid> boids) {
    PVector steer = new PVector(0,0,0);
    int count = 0;
    // For every boid in the system, check if it's too close
    for (Boid other : boids) {
      float d = PVector.dist(position,other.position);
      // If the distance is greater than 0 and less than an arbitrary amount (0 when you are yourself)
      if ((d > 0) && (d < desiredseparation)) {
        // Calculate vector pointing away from neighbor
        PVector diff = PVector.sub(position,other.position);
        diff.normalize();
        diff.div(d);        // Weight by distance
        steer.add(diff);
        count++;            // Keep track of how many
      }
    }
    // Average -- divide by how many
    if (count > 0) {
      steer.div((float)count);
    }

    // As long as the vector is greater than 0
    if (steer.mag() > 0) {
      // Implement Reynolds: Steering = Desired - Velocity
      steer.normalize();
      steer.mult(maxspeed);
      steer.sub(velocity);
      steer.limit(maxforce);
    }
    return steer;
  }

  // Alignment
  // For every nearby boid in the system, calculate the average velocity
  PVector align (List<Boid> boids) {
    PVector sum = new PVector(0,0);
    int count = 0;
    for (Boid other : boids) {
      float d = PVector.dist(position,other.position);
      if ((d > 0) && (d < neighbordist)) {
        sum.add(other.velocity);
        count++;
      }
    }
    if (count > 0) {
      sum.div((float)count);
      sum.normalize();
      sum.mult(maxspeed);
      PVector steer = PVector.sub(sum,velocity);
      steer.limit(maxforce);
      return steer;
    } else {
      return new PVector(0,0);
    }
  }

  // Cohesion
  // For the average position (i.e. center) of all nearby boids, calculate steering vector towards that position
  PVector cohesion (List<Boid> boids) {
    PVector sum = new PVector(0,0);   // Start with empty vector to accumulate all positions
    int count = 0;
    for (Boid other : boids) {
      float d = PVector.dist(position,other.position);
      if ((d > 0) && (d < neighbordist)) {
        sum.add(other.position); // Add position
        count++;
      }
    }
    if (count > 0) {
      sum.div(count);
      return seek(sum);  // Steer towards the position
    } else {
      return new PVector(0,0);
    }
  }
  
  //Attract - Repell Persons
  PVector attractRepell(HashMap<Long, Person> persons) {
    attractorDistanceStrength = 0;
    PVector vec = new PVector(0,0);
    for(Person person : persons.values()) {
      int personDistance = (int)PVector.dist(position, person.position);
      if(personDistance < distanceThreshhold) { //todo degfine distance constants //&& personDistance > distanceThreshhold / 4
         float distanceForce = personDistance / distanceThreshhold;
         if(person.role == PersonRole.ATTRACTOR) {
           attractorDistanceStrength = max(distanceForce, attractorDistanceStrength);
              PVector vecToAdd = util.getDirectionVector(person, this);           //.rotate(HALF_PI * 0.1 * (1 - distanceForce))
              if(attractorDistanceStrength < 0.25) {
                vecToAdd.rotate(PI);
              }
              vecToAdd.mult(distanceForce * 0.4);
              vec = vec.add(vecToAdd);
           }
         if(person.role == PersonRole.REPELLER) {
           vec = vec.add(util.getDirectionVector(person, this).rotate(PI).mult(1 - distanceForce));
           //TODO add to vector reversed depending on distance
         }
      }
    }
    return vec;
  }
  
  PVector islandSeparation(List<Island> islands) {
    PVector vec = new PVector(0,0);
    for(Island island: islands) {
      float distanceThreshhold = max(island.w + distanceOffset, island.h + distanceOffset);
      int distance = (int)PVector.dist(position, island.position);
      int xDistance = (int)Math.abs(position.x - island.position.x);
      int yDistance = (int)Math.abs(position.y - island.position.y);
      
      if(xDistance < island.w + distanceOffset && yDistance < island.h + distanceOffset) {
          float distanceForce = distance / distanceThreshhold;
          PVector direction = util.getDirectionVector(island, this);
          // float angle = direction.heading();
            direction.rotate(PI).mult(1 - distanceForce);

          vec.add(direction);
      }
    }
    return vec;
  }
  
  
}
