class EffectHandler {
  final static int ISLANDREMOVETIME = 30;
  final static int DROPEFFECTTIME = 500;
  int removeAnimationFrame = 0;
  int dropModeCounter = 0;
  boolean removeIslandMode = false;
  boolean dropMode = false;
  
  void run() {
     //println(song.position());
    for (int i = 0; i < musicFile.dropTime.length; i++) {
      if (song.position() > musicFile.dropTime[i] && song.position() < musicFile.dropTime[i] + 200){
        if(!dropMode) {
          isDrop();
          return;
        }
      }
      
      checkIslandRemoveAnimation();
      checkDropMode();
    }
  }
  
  void isDrop() {
    dropMode = true;
    shouldAttract = false;
    removeIslandMode = true;
    changePersonRoles();
  }
  
  void checkIslandRemoveAnimation() {
    if(!removeIslandMode) {
      return;
    }
    if(removeAnimationFrame > ISLANDREMOVETIME) {
      removeAnimationFrame = 0;
      removeIslandMode = false;
      islandHandler.removeIslands();
    }
    
    if(removeAnimationFrame < ISLANDREMOVETIME * 0.4) {
      islandHandler.resizeIslands(1.05 - removeAnimationFrame * 0.001, false);
    } else {
      islandHandler.resizeIslands(0.93 - (ISLANDREMOVETIME - removeAnimationFrame) * 0.001, true);
    }
    
    removeAnimationFrame++;
  }
  
  void checkDropMode() {
    dropModeCounter++;
    
    if(dropModeCounter > EffectHandler.DROPEFFECTTIME) {
      dropMode = false;
      shouldAttract = true;
      dropModeCounter = 0;
    }
  }
  
  void changePersonRoles() {
     for(Person person : pc.persons.values()) {
       if(person.role == PersonRole.ATTRACTOR) {
         person.role = PersonRole.REPELLER;
       } else {
         person.role = PersonRole.ATTRACTOR;
     }
    }
  }
}
