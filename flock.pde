class Flock {
  final static int GRID_SIZE = 10;
  List<Boid> boids;

  Flock() {
    boids = new ArrayList<Boid>();
  }

  void run() {
    //resetFlockGrid();
    //populateFlockGrid();
    
    for (Boid b : boids) {
      b.run(flock, islandHandler.islands, pc.persons);
    }
  }

  void addBoid(Boid b) {
    boids.add(b);
  }

}
