class Island extends PositionObject {
  final static int MAX_AGE = 5000;
  int age;
  float opacity;
  int visualisationType = 0; // soll visualisierung bestimmen
  int baseW;
  int baseH;
  int w;
  int h;
  float angle;
  
  Island(int visualisationType, int x, int y, int w, int h, float angle) {
    super(x,y);
    this.age = 0;
    this.baseW = this.w = w;
    this.baseH = this.h = h;
    this.visualisationType = visualisationType;
    this.angle = angle;
    this.opacity = 1;
  }
  
  void run() {
    this.w = this.baseW + (int)(audioAnalyzer.spectrum[4] * this.opacity);
    this.h = this.baseH + (int)(audioAnalyzer.spectrum[5] * this.opacity);
    this.age++;
    this.opacity = 1 - (this.age / (float)Island.MAX_AGE);
  }
  
}
