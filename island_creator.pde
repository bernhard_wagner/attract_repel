class IslandCreator {
  public int DISTANCE_THRESHOLD = 230 / scaleFactor;
  
  void run() {
    for (Person person : pc.persons.values()) {
      person.islandRun();
      if (person.role == PersonRole.ATTRACTOR){
        person.updateConnections();
        if (person.connectedPerson != null){
          if(person.connectedPersonDistance < DISTANCE_THRESHOLD && !person.islandCreationMode) {
            person.noConnectionAllowed = true;
            person.islandCreationMode = true;
            person.islandCreationTime = 0;
            person.island = islandHandler.addIsland((int)random(4),-100,-100, person.connectedPersonDistance, person.connectedPersonDistance, 0.); //TODO instead of 2 use random value
          }
        }  
      }
    }
  }
}
