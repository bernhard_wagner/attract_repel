class IslandHandler {
  final static int MAX_ISLANDS = 10;
  int islandNumber = 0;
  ArrayList<Island> islands = new ArrayList<Island>();

  Island addIsland(int visualisationType, int x, int y, int w, int h, float a) {
    if (islandNumber == MAX_ISLANDS) {
      return null;
    }
    Island island = new Island(visualisationType, x, y, w, h, a);
    islands.add(island);
    islandNumber++;
    return island;
  }

  void run() {
    for (Iterator<Island> itr = islands.iterator(); itr.hasNext(); ) {            
      Island island = itr.next();
      if (island.age > Island.MAX_AGE) {
        islands.remove(island);
      }

      island.run();
    }
  }
  
  void removeIslands() {
    islands.clear();
  }
  
  void resizeIslands(float value, boolean changeAge) {
    for (Iterator<Island> itr = islands.iterator(); itr.hasNext(); ) {            
      Island island = itr.next();
      
      island.baseW *= value;
      island.baseH *= value;
      
      if(changeAge && island.age < Island.MAX_AGE - 1000) {
        island.age += 2;
      }
    }
  }
}
