
int[] tocattaDrop = {30000, 180000, 320400}; //60000
public Music tocatta = new Music("/songs/tocatta.mp3", tocattaDrop);

int[] merciDrop = {};
public Music merci = new Music("/songs/merci.mp3", merciDrop);

int[] suicideDrop = {28800, 123900};
public Music suicide = new Music("/songs/suicide.mp3", suicideDrop);

int[] fluteDrop = {28800, 123900};
public Music flute = new Music("/songs/flute.mp3", fluteDrop);

int[] fairDrop = {29000, 57000, 115000};
public Music fair = new Music("/songs/fair.mp3", fairDrop);

int[] weinDrop = {};
public Music wein = new Music("/songs/wein.mp3", weinDrop);

int[] youDrop = {64000, 115500, 166780, 218000, 273500};

public Music you = new Music("/songs/you.mp3", youDrop);


public class Music {

  String musicFile; //location
  int[] dropTime; //in ms
  
  //constructor
  public Music(String musicFileLocation, int [] dropTimePosition){
    this.musicFile = musicFileLocation;
    this.dropTime = dropTimePosition;
  }  
}
