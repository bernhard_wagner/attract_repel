enum PersonRole {
  ATTRACTOR,
  REPELLER
}

class Person extends PositionObject {
  final static int ISLAND_CREATION_TIMEOUT = 130;
  final static int NO_CONNECTION_ALLOWED_TIMEOUT = 650;
  final static int STANDING_ACTION_TIMEOUT = 120;
  final static int STANDING_POSITION_THRESHHOLD = 25;
  int PERSON_CONNECTION_THRESHHOLD = 370 / scaleFactor; //pixel
  PVector standingPoint;
  int id;
  long tuioId;
  int age;
  PersonRole role;
  PharusClient pc;
  int standingAge;
  ArrayList<Foot> feet = new ArrayList<Foot>();
  Person connectedPerson;
  int connectedPersonDistance;
  boolean islandCreationMode;
  boolean noConnectionAllowed;
  int islandCreationTime = 0;
  Island island;
  //TODO cooldown time so you cannot make 100 islands at once
  
  Person(PharusClient pc, int id, long tuioId,  int x,int y, PersonRole role) {
    super(x, y);
    this.id = id; //must be id from helping function GetCursorID
    this.tuioId = tuioId;
    this.role = role;
    this.pc = pc;
    this.standingAge = 0;
    this.standingPoint = new PVector(x, y);
    this.islandCreationMode = false;
    this.noConnectionAllowed = false;
  }
  
   boolean isJumping() {  
    return feet.size() == 0 && age > 1;
  }
  
  void executeJumpingAction() {
    connectedPerson = null;
    role = PersonRole.REPELLER;
  }
  
  void standingAction() {
    connectedPerson = null;
    role = PersonRole.ATTRACTOR;
  }
  
  void updateConnections() {
    if(noConnectionAllowed || islandCreationMode || islandCreationTime > 0) {
      return;
    }
    for(Person otherPerson : pc.persons.values()){
      if(otherPerson.role == PersonRole.REPELLER){
        int distance = (int)this.position.dist(otherPerson.position);
        if(distance < PERSON_CONNECTION_THRESHHOLD) {
          connectedPerson = otherPerson;
          connectedPersonDistance = distance;
          return;
        }
      }
    }
    
    connectedPerson = null;
  }
  
  void islandRun() {
     if(islandCreationTime > NO_CONNECTION_ALLOWED_TIMEOUT) {
       islandCreationTime = ISLAND_CREATION_TIMEOUT + 1;
       return;
     }
    
    islandCreationTime++;
       
    if(islandCreationTime > ISLAND_CREATION_TIMEOUT || island == null) {
      islandCreationMode = false;
      island = null;
      islandCreationTime = 0;
      connectedPerson = null;
      return;
    }
    
    int distance = min((int)this.position.dist(connectedPerson.position), 190 / scaleFactor);
    PVector centerPoint = new PVector(this.position.x, this.position.y).add(connectedPerson.position).div(2);
    island.position.x = centerPoint.x;
    island.position.y = centerPoint.y;
    island.angle = util.getBearing(this.position, centerPoint);
    island.baseW = distance / 2;
    island.baseH = distance;
  }
}

class Foot extends PositionObject
{
  public Foot(int x, int y) {
    super(x,y);
  }
}
