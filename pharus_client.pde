
// Version 3.1
// this class replaces former TUIOHelper in this new and enhanced implementation
// do not change anything in here unless you are really sure of what you are doing

import java.lang.*;
import java.lang.reflect.*;
import processing.core.*;
import TUIO.*;
import java.util.*;

// reference: https://github.com/processing/processing/wiki/Library-Basics
public class PharusClient extends PApplet 
{
  // internal stuff only
  PApplet parent;
  TuioProcessing tuioProcessing;
  int nextUniqueID = 0;
  Method PersonAddEventMethod;
  Method PersonRemoveEventMethod;

  boolean doDebug = false;  
  int maxAge = 50; // age is measured in update cycles, with 25 fps this is 2 seconds
  float jumpDistanceMaxTolerance = 0.05f; // max distance allowed when jumping between last known position and potential landing position, unit is in pixels relative to window width
  HashMap<Long, Person> persons = new HashMap<Long, Person>(); //effizienter -> direkter zugriff auf Person //TODO in unserem example Person austauschen
  PharusClient(PApplet _parent)  {
    this.parent = _parent;
    // this is a bit of a hack but need for TuioProcessing to work properly
    width = parent.width;
    height = parent.height;
    tuioProcessing = new TuioProcessing(this); 

    parent.registerMethod("dispose", this);
    parent.registerMethod("pre", this); 

    // check to see if the host applet implements event functions
    try {
      PersonAddEventMethod = parent.getClass().getMethod("pharusPersonAdded", new Class[] { Person.class });
    } 
    catch (Exception e) {
      // no such method, or an error..
      println("Function pharusPersonAdded() not found, Person add events disabled.");
    }
    try 
    {
      PersonRemoveEventMethod = parent.getClass().getMethod("pharusPersonRemoved", new Class[] { Person.class });
    } 
    catch (Exception e) 
    {
      // no such method, or an error..
      println("Function pharusPersonRemoved() not found, Person remove events disabled.");
    }    
  }
  
  // age is measured in update cycles, with 25 fps this is 2 seconds
  public void setMaxAge(int age) {
    maxAge = age;
  }

  // max distance allowed when jumping between last known position and potential landing position, unit is in pixels relative to window width
  public void setjumpDistanceMaxTolerance(float relDist) {
    jumpDistanceMaxTolerance = relDist;
  }

  void firePersonAddEvent(Person Person)  {
    if (PersonAddEventMethod != null)  {
      try  {
        PersonAddEventMethod.invoke(parent, new Object[] { Person });
      } 
      catch (Exception e)  {
        System.err.println("Disabling event for " + PersonAddEventMethod.getName() + " because of an error.");
        e.printStackTrace();
        PersonAddEventMethod = null;
      }
    }
  }
  
  void firePersonRemoveEvent(Person Person) {
    if (PersonRemoveEventMethod != null) {
      try  {
        PersonRemoveEventMethod.invoke(parent, new Object[] { Person });
      } 
      catch (Exception e) {
        System.err.println("Disabling event for " + PersonRemoveEventMethod.getName() + " because of an error.");
        e.printStackTrace();
        PersonRemoveEventMethod = null;
      }
    }
  }

  public void dispose() {
    // Anything in here will be called automatically when the parent sketch shuts down.
    persons.clear();
    tuioProcessing.dispose();
  }

  public void pre() {
    // Method that's called just after beginDraw(), meaning that it can affect drawing.

    int m = millis();

    // Increase age of all persons, remove those too old
    Iterator<HashMap.Entry<Long, Person>> iter = persons.entrySet().iterator();
    while (iter.hasNext())  {
      HashMap.Entry<Long, Person> personsEntry = iter.next();
      Person p = (Person)personsEntry.getValue();
      p.age++;
      // remove if too old
      if (p.age > maxAge) {
          firePersonRemoveEvent(p);
          iter.remove();
      }
    }
    
    // Update known persons with available tuio data
    ArrayList<TuioCursor> tcl = tuioProcessing.getTuioCursorList();
    int i = 0;
    while (i < tcl.size()) {
      TuioCursor tc = tcl.get(i);
      Person p = persons.get(tc.getSessionID());
      if (p != null) {
        // update Person
        p.age = 0;
        int newX = tc.getScreenX(width);
        int newY =  tc.getScreenY(wallHeight);
        
        //check if standing action should be executed
        if(p.standingAge > Person.STANDING_ACTION_TIMEOUT) {
          p.standingAction();
          p.standingAge = 0;
          p.standingPoint = new PVector(newX, newY);
        }
        else if(p.standingPoint.dist(new PVector(newX, newY)) < Person.STANDING_POSITION_THRESHHOLD) {
          p.standingAge++;
        } else {
          p.standingAge = 0;
          p.standingPoint = new PVector(newX, newY);
        }
        p.position.x = newX;
        p.position.y = newY;
        tcl.remove(i);
      }
      else {
        i++;
      }
    }

    // check if there are new persons left and handle them accordingly 
    if (tcl.size() > 0) {
      for (TuioCursor tc : tcl) {
        boolean found = false;
        // check if this was a previously known Person with different id (TUIO id swap case due to jumping e.g.)
        for (HashMap.Entry<Long, Person> personsEntry : persons.entrySet()) {
          Person p = personsEntry.getValue();
          if (p.age == 0) {
            continue;
          }
          if (dist(p.position.x, p.position.y, tc.getScreenX(width), tc.getScreenY(height)) < jumpDistanceMaxTolerance * width) {
              // swap previous TUIO id to new id
              println("updating tuio id of Person " + p.id + " from " + p.tuioId + " to " + tc.getSessionID());
              persons.remove(p.tuioId);
              persons.put(tc.getSessionID(), p);
              // update Person
              p.age = 0;
              p.position.x = tc.getScreenX(width);
              p.position.y = tc.getScreenY(height);
              found = true;
              break;
          }
        }
        // add as new Person if nothing found
        if (!found) {
          PersonRole role = PersonRole.ATTRACTOR;
          if(nextUniqueID % 2 == 0) {
            role = PersonRole.REPELLER;
          }
          Person p = new Person(this, nextUniqueID++, tc.getSessionID(), tc.getScreenX(width), tc.getScreenY(height), role);
          persons.put(tc.getSessionID(), p);
          firePersonAddEvent(p);
        }
      }
    }
    
    //update feet
    ArrayList<TuioObject> tuioObjectList = tuioProcessing.getTuioObjectList();
    ArrayList<TuioCursor> tuioCursorList = tuioProcessing.getTuioCursorList();
    for (TuioObject to : tuioObjectList)
    {
      Person p = null;
      if (to.getSymbolID() < tuioCursorList.size())
      {
        TuioCursor tc = tuioCursorList.get(to.getSymbolID());
        p = persons.get(tc.getSessionID());
      }
      if (p != null)
      {
        p.feet.add(new Foot(to.getScreenX(width), to.getScreenY(height)));
        
        if(p.isJumping()) {
          p.executeJumpingAction();
        }
      }
    }
    
  //  println((millis() - m) + " ms update");
  }

  // ------ these callback methods are called whenever a TUIO event occurs ------------------

  // called when an object is added to the scene
  void addTuioObject(TuioObject tobj)  {
    if (doDebug)
      println("add object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle());
  }

  // called when an object is removed from the scene
  void removeTuioObject(TuioObject tobj) {
    if (doDebug)
      println("remove object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+")");
  }

  // called when an object is moved
  void updateTuioObject (TuioObject tobj) {
    if (doDebug)
      println("update object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle()+" "+tobj.getMotionSpeed()+" "+tobj.getRotationSpeed()+" "+tobj.getMotionAccel()+" "+tobj.getRotationAccel());
  }

  // called when a cursor is added to the scene
  void addTuioCursor(TuioCursor tcur) {
    if (doDebug)
      println("add cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") "+tcur.getX()+" "+tcur.getY());
  }

  // called when a cursor is moved
  void updateTuioCursor (TuioCursor tcur) 
  {
    if (doDebug)
      println("update cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY()+" "+tcur.getMotionSpeed()+" "+tcur.getMotionAccel());
  }

  // called when a cursor is removed from the scene
  void removeTuioCursor(TuioCursor tcur) {
    if (doDebug)
      println("remove cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+")");
  }

  // called after each message bundle
  // representing the end of an image frame
  void refresh(TuioTime bundleTime) { 
    redraw();
  }

  void addTuioBlob(TuioBlob tblb) {
    if (doDebug)
      println("add blob "+tblb.getBlobID()+" ("+tblb.getSessionID()+ ") "+tblb.getX()+" "+tblb.getY());
  }

  void removeTuioBlob(TuioBlob tblb) {
    if (doDebug)
      println("remove blob "+tblb.getBlobID()+" ("+tblb.getSessionID()+ ") "+tblb.getX()+" "+tblb.getY());
  }

  void updateTuioBlob(TuioBlob tblb) {
    if (doDebug)
      println("update blob "+tblb.getBlobID()+" ("+tblb.getSessionID()+ ") "+tblb.getX()+" "+tblb.getY());
  }
}
