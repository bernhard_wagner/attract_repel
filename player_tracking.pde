
// Version 3.1
// This example uses PharusClient class to access pharus data
// Pharus data is encapsulated into Player objects
// PharusClient provides an event callback mechanism whenever a player is been updated

boolean showPath = false;
boolean showTrack = true;
boolean showFeet =  false;

int personSize = 80 / scaleFactor;

void initPlayerTracking()
{
  pc = new PharusClient(this);
  // age is measured in update cycles, with 25 fps this is 2 seconds
  pc.setMaxAge(50);
  // max distance allowed when jumping between last known position and potential landing position, unit is in pixels relative to window width
  pc.setjumpDistanceMaxTolerance(0.05f);  
}

void drawFPS() {
   fill(255);
   text((int)frameRate + " FPS", 25, 10);
}

void drawPlayerTracking()
{
  // reference for hashmap: file:///C:/Program%20Files/processing-3.0/modes/java/reference/HashMap.html
  for (HashMap.Entry<Long, Person> playersEntry : pc.persons.entrySet()) 
  {
    Person p = playersEntry.getValue();

    //// render path of each track
    //if (showPath)
    //{
    //  if (p.getNumPathPoints() > 1)
    //  {
    //    stroke(70, 100, 150, 25.0f );        
    //    int numPoints = p.getNumPathPoints();
    //    int maxDrawnPoints = 300;
    //    // show the motion path of each track on the floor    
    //    float startX = p.getPathPointX(numPoints - 1);
    //    float startY = p.getPathPointY(numPoints - 1);
    //    for (int pointID = numPoints - 2; pointID > max(0, numPoints - maxDrawnPoints); pointID--) 
    //    {
    //      float endX = p.getPathPointX(pointID);
    //      float endY = p.getPathPointY(pointID);
    //      line(startX, startY, endX, endY);
    //      startX = endX;
    //      startY = endY;
    //    }
    //  }
    //}

    // render tracks = player
    if (showTrack)
    {
      // show each track with the corresponding  id number
      noStroke();
      if (p.isJumping())
      {
        pg.fill(255, 255, 255);
      }
      else
      {
        if(p.role == PersonRole.REPELLER) {
          pg.fill(70, 60, 200);
        } else {
           pg.fill(255, 255, 255);
        }
      }
      pg.ellipse(p.position.x, p.position.y, personSize, personSize);
      pg.fill(0);
      pg.text(p.id /*+ "/" + p.tuioId*/, p.position.x, p.position.y);
    }

    // render feet for each track
    if (showFeet)
    {
      // show the feet of each track
      pg.stroke(70, 100, 150, 200);
      pg.noFill();
      // paint all the feet that we can find for one character
      for (Foot f : p.feet)
      {
        pg.ellipse(f.position.x, f.position.y, personSize / 3, personSize / 3);
      }
    }
  }
}

void pharusPlayerAdded(Person person) {
  println("Player " + person.id + " added");
  
  // TODO do something here if needed
}

void pharusPlayerRemoved(Person person)
{
  println("Player " + person.id + " removed");
  
  // TODO do something here if needed  
}
