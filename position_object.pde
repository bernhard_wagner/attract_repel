abstract class PositionObject {
    PVector position;
  
  PositionObject(int x, int y) {
    this.position = new PVector(x, y);
  }
  
  void setPosition(int x, int y) {
    this.position.x = x;
    this.position.y = y;
  }
}
