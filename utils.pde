class Utils {
  int calcSquareDistance(PositionObject obj1, PositionObject obj2) {
    return (int)(pow(obj2.position.x - obj1.position.x, 2) + pow(obj2.position.y - obj1.position.y, 2));
  }
  
  int calcSquareDistance(PVector v1, PVector v2) {
    return (int)(pow(v2.x - v1.x, 2) + pow(v2.y - v1.y, 2));
  }
  
  PVector getDirectionVector(PositionObject obj1, PositionObject obj2) {
    PVector vec = new PVector(obj1.position.x, obj1.position.y);
    return vec.sub(obj2.position).normalize();
  }
  
  float getBearing(PVector v1, PVector v2) {
    float theta = atan2(v2.x - v1.x, v1.y - v2.y);
    if (theta < 0.0) {
        theta += TWO_PI;
    }
    return theta;
  }
}
