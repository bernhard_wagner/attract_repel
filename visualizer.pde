class Visualizer {
  private AudioAnalyzer audioAnalyzer;
  private Flock flock = new Flock();
  private PShape bgImage;
  private boolean drawBg = true;

  PShape[] islandShapes = {
    loadShape("./images/Insel_01.svg"), //TODO lisa
    loadShape("./images/Insel_02.svg"),
    loadShape("./images/Insel_03.svg"),
    loadShape("./images/Insel_04.svg"),
  };

  //TOTO Background in different size
  //bzier detail!! when drawing bezier curve

  Visualizer(AudioAnalyzer audioAnalyzer, Flock flock) {
    this.audioAnalyzer = audioAnalyzer;
    this.flock = flock;
    init();
  }

  void visualize() {
    //pg.blendMode(NORMAL);
    //pg.background(0);
    flockPg.beginDraw();
      drawBackgroundImage();
      drawFlock();
    flockPg.endDraw();
    drawIslands();
    drawPersons();
  }

  private void drawBackgroundImage() {
    //drawGradient(0,0,windowWidth, wallHeight, color(180+(audioAnalyzer.score.low * 1),255,255, 0.4), color(200,50,10, audioAnalyzer.score.mid));
    //flockPg.fill(audioAnalyzer.score.low);
    //flockPg.rect(0,0,windowWidth, wallHeight);
    //flockPg.tint(255, 75);
    //flockPg.image(bgImage, 0,0);
    flockPg.noStroke();
    flockPg.fill(0, 170 - audioAnalyzer.score.high * 3);
    flockPg.rect(0,0,windowWidth, wallHeight);
    
    flockPg.noFill();
    flockPg.stroke(150+(audioAnalyzer.score.low * 1 - 20), 40+(audioAnalyzer.score.low * 1), 80+(audioAnalyzer.score.low * 1), 80 + audioAnalyzer.score.mid); //TODO do something with colors
    flockPg.shape(bgImage, 0, 0, windowWidth, wallHeight);

  }

  private void drawFlock() {
    flockPg.colorMode(HSB);
    if(effectHandler.dropMode) {
       flockPg.blendMode(ADD);
    } else {
      flockPg.blendMode(LIGHTEST);
    }
    for (Boid boid : flock.boids) {
      float theta = boid.velocity.heading();
      float colorness = boid.attractorDistanceStrength > 0 ? 1 - min(boid.attractorDistanceStrength, 1) : 0; //wert zwischen 1 und 0 wie nahe an attractor
      //println(colorness);
      //pg.fill(min(140 + audioAnalyzer.score.low * 1, 255), 100 + colorness * 155, 100 + (colorness * 155));
      flockPg.stroke(min(140 + audioAnalyzer.score.mid * 1, 360), 100 + colorness * 155, 200 + (colorness * 55));
      flockPg.noFill();

      flockPg.pushMatrix();

      flockPg.translate(boid.position.x, boid.position.y);
      flockPg.rotate(theta);

      //pg.shape(boidShape, 0,0);
      //pg.point(0,0);
      //pg.point(1,1);
      //pg.point(0,1);
      //pg.point(1,0);
      //pg.point(3,0);
      //pg.point(4,0);
      //pg.point(4,1);
      flockPg.strokeWeight(1);
      flockPg.fill(min(160 + audioAnalyzer.score.low * 1, 255), 100 + colorness * 155, 10 + (colorness * 100), 255);
      flockPg.ellipse(0, 0, boid.r, boid.r / 3);
      //flockPg.ellipse(2, 0, 5, 2);
      //pg.fill(360, 100, 100);
      //pg.stroke(360, 100, 100);
      //pg.ellipse(1.5, 0, 1, 1);
      try {
        if (boid.lastPosition != null) {
          PVector pos = boid.lastPosition;
          PVector relativePos = new PVector(pos.x, pos.y).sub(boid.position).rotate(-theta);
          flockPg.noStroke();
          flockPg.pushMatrix();
          flockPg.translate(relativePos.x, relativePos.y); // TODO performance
          flockPg.fill(min(160 + audioAnalyzer.score.low * 1, 255), 100 + colorness * 155, 50 + (colorness * 100), 60);
          flockPg.ellipse(0, 0, boid.r - 10, boid.r / 5);
          flockPg.popMatrix();
        }
      } 
      catch(Throwable e) {
      }

      flockPg.popMatrix();
    }
    flockPg.blendMode(NORMAL);

    pg.noStroke();
  }

  void drawIslands() {
    try {
      for (Island island : islandHandler.islands) {
        PShape islandShape = islandShapes[island.visualisationType]; //TODO auswählen nach island type
        islandShape.disableStyle();
        //pg.blendMode(ADD); //rausgeben wenn nicht passt
        pg.pushMatrix();
          pg.translate(island.position.x, island.position.y);
          pg.rotate(island.angle);
          pg.fill(170+(audioAnalyzer.score.low * 1 - 20), 255, 255, 70 * island.opacity);
          pg.stroke(190+(audioAnalyzer.score.low * 1 - 20), 255, 255, 55 + 200 * island.opacity); //TODO do something with colors
          pg.shape(islandShape, 0, 0, min(island.w, island.baseW + 60), min(island.h, island.baseH + 60));
        pg.popMatrix();
      }
    } 
    catch(Throwable e) {
    }
  }

  void drawPersons() {
    pg.stroke(255, 100);
    pg.strokeWeight(1);
    for (Person p : pc.persons.values()) {
      //TODO -> draw persons here and remove player tracking
      if (p.role == PersonRole.ATTRACTOR) {
            pg.fill(170+(audioAnalyzer.score.low * 1 - 20), 255, 255, 70);
            pg.stroke(100,0, 255);
          } else {
                  pg.fill(80, 0, 255);
                  pg.stroke(170+(audioAnalyzer.score.low * 1 - 20), 100, 80);
                }

      pg.ellipse(p.position.x, p.position.y, 15 / scaleFactor, 15 / scaleFactor);

      if (p.connectedPerson != null) {
        pg.line(p.position.x, p.position.y, p.connectedPerson.position.x, p.connectedPerson.position.y);
      }
    }
    pg.strokeWeight(1);
  }

  private void drawGradient(int x, int y, float w, float h, color c1, color c2) {
    flockPg.noFill();
    for (int i = y; i <= y+h; i++) {
      float inter = map(i, y, y+h, 0, 1);
      color c = lerpColor(c1, c2, inter);
      flockPg.stroke(c);
      flockPg.line(x, i, x+w, i);
    }
  }


  private void init() {
    bgImage = loadShape(backgroundImageFile);
    bgImage.disableStyle();  // Ignore the colors in the SVG
    //PShape test = bgImage.getChild(0);
    //test.enableStyle();
    //test.setFill(color(0, 10));
  }
}
